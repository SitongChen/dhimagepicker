import React, {Component} from 'react'
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  Platform
} from 'react-native'

import SwiftView from './SwiftView'

export default class SwiftWrapperView extends React.Component{
  componentWillUnmount(){
    console.log("componentWillUnmount")
  }

  _onChange(event){
    console.log("OOOO Yep get event from Native iOS.")
    console.log(event.nativeEvent)
  }

  render() {
    return (
      <View style={{flex: 1}}>
        {
          Platform.OS == 'ios' ?
           <SwiftView
                  imageLocalIdentifiers = {this.props.imageLocalIdentifiers}
                  onChange={this._onChange}
                  style={{flex: 1}} /> :
          <View><Text>This is Android</Text></View>
        }
      </View>


    );
  }
}
