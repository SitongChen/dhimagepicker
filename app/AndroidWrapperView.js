import React, {Component} from 'react'
import {
  View,
  Text,
  Image,StyleSheet,Dimensions,
  TouchableHighlight,
} from 'react-native'
import ProgressBar from './AndroidView'

export default class AndroidWrapperView extends React.Component{
  render() {
    return (
      <View style={{flex: 1}}>
        <ProgressBar
          progress={0}
          indeterminate={true}
          style={styles.progressBar }/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  progressBar: {
    height: 50,
    // flex: 1,
    margin: 20,
    width: Dimensions.get('window').width - 2 * 20,
    backgroundColor: 'black',
  },
});
