import React, {Component} from 'react'
import {
  View,
  Text,
  Image,
  TouchableHighlight,
} from 'react-native'

import AndroidCropView from './AndroidCropView'
import TestView from './TestView'


export default class AndroidCropScreen extends React.Component{
  render() {
    return (
      <View style={{flex:1}}>
        <AndroidCropView
          imageUrlList = { this.props.imageUrlList }
          style={{flex: 1, backgroundColor: "black"}} />

      </View>
    );
  }
}
