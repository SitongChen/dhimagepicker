import {requireNativeComponent, View} from 'react-native';
import PropTypes from 'prop-types';

var iface = {
    name: 'TestView',
    PropTypes: {
        ...View.propTypes // include the default view properties
    }
}
module.exports = requireNativeComponent('TestView', null); //Add props are bit different.
