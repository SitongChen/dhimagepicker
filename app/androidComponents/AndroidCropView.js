import {requireNativeComponent, View} from 'react-native';
import PropTypes from 'prop-types';

var iface = {
    name: 'ImageCropView',
    PropTypes: {
        imageUrlList: PropTypes.array,
        ...View.propTypes // include the default view properties
    }
}
module.exports = requireNativeComponent('ImageCropView', null); //Add props are bit different.
