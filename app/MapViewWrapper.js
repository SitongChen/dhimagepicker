import React, {Component} from 'react'
import {
  View,
  Text,
  Image,
  TouchableHighlight,
} from 'react-native'

import MapView from './MapView'

export default class MapWrapperView extends React.Component{
  render() {
    return (
      <MapView style={{flex: 1}} />
    );
  }
}
