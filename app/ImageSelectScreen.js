import React, {Component} from 'react'
import ImagePicker from 'react-native-image-crop-picker';
import {Navigation} from 'react-native-navigation'
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  Platform
} from 'react-native'

export default class ImageSelectScreen extends React.Component{

  constructor(props){
    super(props)
    this.onTap = this.onTap.bind(this)
  }
  onTap(){
    ImagePicker.openPicker({
      multiple: true
    }).then(images => {
      console.log(images);
      if (Platform.OS == "ios"){
        console.log("This is a iPhone.")
        const imageLocalIdentifiers = images.map((item) => {
          return item.localIdentifier
        })
        this.props.navigator.push({
          screen: "st.SwiftWrapperView",
          title: "CROPPING",
          passProps : { imageLocalIdentifiers: imageLocalIdentifiers}
        })
      }
      else{
        console.log("This is a Android.")
        console.log(images)
        const urls = images.map((item) => {
          return item.path
        })
        console.log(urls)
        this.props.navigator.push({
          screen: "st.AndroidCropScreen",
          title: "Android",
          passProps : { imageUrlList : urls}
        })


      }
    });
  }

  showCrop(){
    this.props.navigator.push({
      screen: "st.AndroidCropScreen",
      title: "Android",
      passProps : {}
    })
  }
  render() {
    return (
      <View>
        <TouchableHighlight onPress= {this.onTap.bind(this)}>
          <Text> Tap Me ! </Text>
        </TouchableHighlight>
        <TouchableHighlight onPress= {this.showCrop.bind(this)}>
          <Text> Tap OK ! </Text>
        </TouchableHighlight>
      </View>
    );
  }
}
