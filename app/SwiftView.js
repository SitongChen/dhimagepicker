import { requireNativeComponent } from 'react-native';

// requireNativeComponent automatically resolves 'RNTCroppingView' to 'RNTCroppingViewManager'
module.exports = requireNativeComponent('RNTCroppingView', null);
