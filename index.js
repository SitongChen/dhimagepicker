import { AppRegistry } from 'react-native';
import App from './App';

import SwiftWrapperView from './app/SwiftWrapperView'
import  MapViewWrapper from './app/MapViewWrapper.js'
import  ImageSelectScreen from './app/ImageSelectScreen.js'
import AndroidWrapperView from './app/AndroidWrapperView'
import AndroidCropScreen from './app/androidComponents/AndroidCropScreen'


// AppRegistry.registerComponent('image_picker', () => App);


import { Navigation } from 'react-native-navigation';

Navigation.registerComponent(
  'st.AndroidCropScreen',
  () => AndroidCropScreen,
);

Navigation.registerComponent(
  'st.AndroidWrapperView',
  () => AndroidWrapperView,
);

Navigation.registerComponent(
  'st.SwiftWrapperView',
  () => SwiftWrapperView,
);
Navigation.registerComponent(
  'st.MapViewWrapper',
  () => MapViewWrapper,
);

Navigation.registerComponent(
  'st.ImageSelectScreen',
  () => ImageSelectScreen,
);

Navigation.startSingleScreenApp({
  screen: {
    screen: 'st.ImageSelectScreen', // unique ID registered with Navigation.registerScreen
    title: 'SELECT IMAGES', // title of the screen as appears in the nav bar (optional)
    navigatorStyle: {}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
    navigatorButtons: {} // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
  },

});
