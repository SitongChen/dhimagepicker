//
//  UIView+Addition.swift
//  image_picker
//
//  Created by Sitong Chen on 2018-03-26.
//  Copyright © 2018 Facebook. All rights reserved.
//

import UIKit

extension UIView{
  func traverResponderChainForViewController() -> UIViewController? {
    let firstResponder = self
    var nextResponder: UIResponder? = firstResponder.next
    print("Next responder")
    print(nextResponder)
    

    while nextResponder != nil{
      print("Next responder")
      if let viewController = nextResponder as? UIViewController{
        print("OOO i get view controller")

        return viewController
      }
      else{
        nextResponder = nextResponder!.next
      }
    }
    return nil
  }
  
  func getParentView() {
    print(self.superview)
    if self.next != nil{
      print("Jesus")
    }
    if let view = self.superview{
      view.getParentView()
    }
  }
}
