//
//  DHImageManager.swift
//  image_picker
//
//  Created by Sitong Chen on 2018-03-25.
//  Copyright © 2018 Facebook. All rights reserved.
//

import UIKit
import Photos

class DHImageManager: NSObject {
  private var imageList : [UIImage] = []
  
  var length: Int{
    return self.imageList.count
  }
  
  
  func updateUrl(urls: [String]) {
    self.imageList = []
    
    let requestOption = PHImageRequestOptions()
    requestOption.isSynchronous = true
    requestOption.resizeMode = .exact
    let assets = PHAsset.fetchAssets(withLocalIdentifiers: urls, options: nil)
    for i in 0..<assets.count{
      let asset = assets.object(at: i)
      PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: requestOption, resultHandler: { (image, resut) in
        if let image = image{
          self.imageList.append(image)
        }
      })
    }

  }
  
  func updateImage(AtIndex index : Int, ByImage image: UIImage) {
    self.imageList[index] = image
  }
  
  func getImage(AtIndex index: Int) -> UIImage? {
    if (index >= self.imageList.count){
      return nil
    }
    return self.imageList[index]
  }
}
