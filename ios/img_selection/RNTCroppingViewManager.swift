//
//  RNTCroppingViewManager.swift
//  image_picker
//
//  Created by Sitong Chen on 2018-03-23.
//  Copyright © 2018 Facebook. All rights reserved.
//

import UIKit

@objc(RNTCroppingViewManager)
class RNTCroppingViewManager : RCTViewManager {
  
  var currentView: UIView?
  
  override func view() -> UIView! {
    return RNTCroppingView();
  }
}
