//
//  DHCollectionViewCell.swift
//  image_picker
//
//  Created by Sitong Chen on 2018-03-23.
//  Copyright © 2018 Facebook. All rights reserved.
//

import UIKit

class DHCollectionViewCell: UICollectionViewCell {
  
  static let IDENTIFIER = "DHCollectionViewCell"

    @IBOutlet weak var imgView: UIImageView!
    
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setImage(image: UIImage) {
        self.imgView.image = image
    }

}
