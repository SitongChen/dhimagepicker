//
//  RNTCroppingView.swift
//  image_picker
//
//  Created by Sitong Chen on 2018-03-23.
//  Copyright © 2018 Facebook. All rights reserved.
//

import UIKit
import Photos

class RNTCroppingView: UIView {

  
  var view: UIView!
  
  // override initializers
  override init(frame: CGRect) {
    super.init(frame: frame)
    xibSetup()
    
    initUI()
    
    DispatchQueue.main.async {
      self.initNavRightButton()
    }
//    initNavRightButton()
    
//    timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(initNavRightButton), userInfo: nil, repeats: true)
    
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    xibSetup()
  }
  
  func xibSetup() {
    view = loadViewFromNib()
    view.frame = bounds
    view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
    addSubview(view)
    
  }
  
  func loadViewFromNib()-> UIView{
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: "RNTCroppingView", bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    return view
  }
    
  func  initUI(){
    self.imageCollectionView.delegate = self
    self.imageCollectionView.dataSource = self
    let nib = UINib.init(nibName: "DHCollectionViewCell", bundle: nil)
    self.imageCollectionView.register(nib, forCellWithReuseIdentifier: "DHCollectionViewCell")
  }
  
  func initNavRightButton(){

//    print(self.getParentView())
    guard let viewcontroller = self.traverResponderChainForViewController() else{
      return
    }
    // Add right nav barButton
    print(viewcontroller)
    
    let rightBarButton = UIBarButtonItem.init(title: "DONE", style: .done, target: self, action: #selector(submitAction))
    viewcontroller.navigationItem.setRightBarButtonItems([rightBarButton], animated: true)
    viewcontroller.navigationController?.navigationBar.backgroundColor = UIColor.red
  }
  

    
  //properties
  
  var onChange: RCTBubblingEventBlock?
  
//  var timer: Timer!
  
  @IBOutlet weak var imageCollectionView: UICollectionView!
  
  @IBOutlet weak var ImageScrollView: FAScrollView!
  
  let imageManager = DHImageManager()
  
  let CellWidth = ((UIScreen.main.bounds.size.width)/3)-1
    
  var name: String = "" {
    didSet{
      print("Get from JS : " + name)
    }
  }
  
  var imageLocalIdentifiers: [String] = [] {
    didSet{
      print("Get ImageLocalIdentifiers from JS : ")
      print(imageLocalIdentifiers)
      self.imageManager.updateUrl(urls: imageLocalIdentifiers)
      if (self.imageManager.length > 0){
        self.imageCollectionView.selectItem(at: IndexPath.init(row: 0, section: 0), animated: true, scrollPosition: .top)
        self.selectImage(AtIndex: 0)
      }
    }
  }
  
  
  func selectImage(AtIndex index: Int) {
    if let image = self.imageManager.getImage(AtIndex: index){
      self.ImageScrollView.imageToDisplay = image
    }
  }
  
  func submitAction() {
    if let blk = self.onChange{
      blk(["Name" : "Sitong"])
    }
  }
  
  @IBAction func cropAction(_ sender: Any) {
    let croppedImage = self.captureVisibleRect()
    
    // Get selected index in the collection view
    if let index = self.imageCollectionView.indexPathsForSelectedItems?.first{
      print("Selected items")
      print(index)
      self.imageManager.updateImage(AtIndex: index.row, ByImage: croppedImage)
      self.imageCollectionView.reloadItems(at: [index])
      self.ImageScrollView.imageToDisplay = croppedImage
    }
  }
    

  
  
  private func captureVisibleRect() -> UIImage{
    
    var croprect = CGRect.zero
    let xOffset = (ImageScrollView.imageToDisplay?.size.width)! / ImageScrollView.contentSize.width;
    let yOffset = (ImageScrollView.imageToDisplay?.size.height)! / ImageScrollView.contentSize.height;

    
    croprect.origin.x = ImageScrollView.contentOffset.x * xOffset;
    croprect.origin.y = ImageScrollView.contentOffset.y * yOffset;
    print("Ratio")
    print(ImageScrollView.contentOffset.x)
    print(ImageScrollView.contentOffset.y)
    
    let normalizedWidth = (ImageScrollView?.frame.width)! / (ImageScrollView?.contentSize.width)!
    let normalizedHeight = (ImageScrollView?.frame.height)! / (ImageScrollView?.contentSize.height)!
    
    croprect.size.width = ImageScrollView.imageToDisplay!.size.width * normalizedWidth
    croprect.size.height = ImageScrollView.imageToDisplay!.size.height * normalizedHeight
    
    let toCropImage = ImageScrollView.imageView.image
    let cr: CGImage? = toCropImage?.cgImage?.cropping(to: croprect)
    let cropped = UIImage(cgImage: cr!)
    
    return cropped
    
  }
}

extension RNTCroppingView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return self.imageManager.length
  }
  
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
      return 1
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell:DHCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: DHCollectionViewCell.IDENTIFIER, for: indexPath) as! DHCollectionViewCell
    
    if let image = self.imageManager.getImage(AtIndex: indexPath.row){
      cell.setImage(image: image)
    }
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize.init(width: CellWidth, height: CellWidth)
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    self.selectImage(AtIndex: indexPath.row)
  }
}
