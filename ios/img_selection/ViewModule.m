//
//  ViewModule.m
//  image_picker
//
//  Created by Sitong Chen on 2018-03-23.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <React/RCTViewManager.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(RNTCroppingViewManager, RCTViewManager)

RCT_EXTERN_METHOD(removeOldView)

RCT_EXPORT_VIEW_PROPERTY(imageLocalIdentifiers, NSArray)



RCT_EXPORT_VIEW_PROPERTY(onChange, RCTBubblingEventBlock)
@end
