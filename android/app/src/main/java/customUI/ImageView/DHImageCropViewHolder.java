package customUI.ImageView;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by sitongchen on 2018-04-17.
 */

public class DHImageCropViewHolder extends Object {

    class BitmapGetter extends AsyncTask<Uri, Void, Bitmap> {

        public BitmapGetter(){

        }
        @Override
        protected Bitmap doInBackground(Uri... uris) {

            if (uris.length < 1){
                return null;
            }
            Uri uri = uris[0];
            File file = new File(uri.getPath());
            if (file.exists()){
                System.out.println("LoadImage");

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                options.inSampleSize = ImageProcesser.calculateInSampleSize(options, VIEWWIDTH , VIEWWIDTH);
                options.inJustDecodeBounds = false;
                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);

                return bitmap;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            cropImageView.setImageBitmap(bitmap);
        }
    }

    public DHImageCropViewHolder( Context c, CropImageView view){
        context = c;
        cropImageView = view;
    }

    public Context context;
    public CropImageView cropImageView;

    public Uri imageUri;

    // The selecte position in the list view. Default is 0.
    public int selectedPosition = 0;
    public int VIEWWIDTH = Resources.getSystem().getDisplayMetrics().widthPixels;


    public void setImageUriForPosition(int position, Uri uri){
        imageUri = uri;
        selectedPosition = position;
        cropImageView.setImageUriAsync(uri);
    }

    public Uri doCropping(){
        Bitmap croppedBitmap = cropImageView.getCroppedImage();
        cropImageView.setImageBitmap(croppedBitmap);

        Uri newUri = dumpImage(croppedBitmap);

        return newUri;
    }

    private Uri dumpImage(Bitmap bitmap){

        // Get image file name.
        String[] path_array = imageUri.getPath().split("/");
        String filename = path_array[path_array.length - 1];
        System.out.println(filename);

        // Get internal dir path.
        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        try {
            File file = new File(context.getFilesDir(), filename);
            System.out.println(file.getAbsoluteFile());
            if (file.exists()){
                return Uri.fromFile(file);
//                System.out.println("Bitmap has been dumped.");
//                if(file.delete()){
//                    System.out.println("Bitmap Deleted.");
//                }else{
//                    System.out.println("Bitmap no deleted.");
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

}
