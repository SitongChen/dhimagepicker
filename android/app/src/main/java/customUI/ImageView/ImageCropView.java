package customUI.ImageView;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;

import com.image_picker.R;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by sitongchen on 2018-03-27.
 */

public class ImageCropView extends ConstraintLayout {

    class BitmapGetter extends AsyncTask<ArrayList<String>, Void, ArrayList<Bitmap>>{

        // Asyn bitmap fetching.

        public BitmapGetter(){

        }
        @Override
        protected ArrayList<Bitmap> doInBackground(ArrayList<String>... params) {

            int cellWIdth = Resources.getSystem().getDisplayMetrics().widthPixels / 3 - 1;

            ArrayList<String> list = params[0];

            ArrayList<Bitmap> bitmapList = new ArrayList<>();

            for(int i = 0; i < list.size(); i++){
                String file_path = list.get(i);
                Uri uri = Uri.parse(file_path);
                System.out.println(uri.getPath());
                File file = new File(uri.getPath());
                if (file.exists()){
                    System.out.println("It exists.");
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                    options.inSampleSize = ImageProcesser.calculateInSampleSize(options, cellWIdth, cellWIdth);
                    options.inJustDecodeBounds = false;
                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                    bitmapList.add(bitmap);
                }
                else{
                    System.out.println("WTF It not exists.");
                }
            }
            return bitmapList;
        }

        @Override
        protected void onPostExecute(ArrayList<Bitmap> bitmapList) {
        }
    }

    private Context context;


    private CropImageView cropImageView;
    private UpdatedRecycleView recyclerView;
    private GridViewAdapter gridViewAdapter;
    private DHImageCropViewHolder cropViewHolder;

    public ImageCropView(Context c){
        super(c);
        this.context = c;
        init();


    }


    // JS passes a list of selected image path to this func.
    public void setImageUrlList(ArrayList list){

        System.out.println("Set Image start.");

        System.out.println("OMG get the image list");
        System.out.println(list);

//        new BitmapGetter().execute(list);

        // Mapping path str into Uris
        ArrayList<Uri> uriArrayList = new ArrayList<>();
        for(int i = 0; i < list.size(); i++) {
            String file_path = (String) list.get(i);
            Uri uri = Uri.parse(file_path);
            uriArrayList.add(uri);
        }
        System.out.println("The uri list");
        System.out.println(uriArrayList);

        gridViewAdapter.updateWholeUriList(uriArrayList);

        return;
    }

    public ImageCropView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public ImageCropView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    public void init(){
        inflate(getContext(), R.layout.image_crop_view_layout, this);

        cropImageView = (CropImageView) findViewById(R.id.cropImageView);
        cropViewHolder = new DHImageCropViewHolder( this.context ,cropImageView);

        recyclerView = (UpdatedRecycleView) findViewById(R.id.my_recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        gridViewAdapter = new GridViewAdapter(context, new GridViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Bitmap bitmap, Uri uri, int position) {
//                cropImageView.setImageBitmap(bitmap);
                cropViewHolder.setImageUriForPosition(position, uri);
            }

        });
        recyclerView.setAdapter(gridViewAdapter);


        // Crop button
        ImageButton cropButton = (ImageButton) findViewById(R.id.cropButton);
        cropButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri newUri = cropViewHolder.doCropping();
                if (newUri != null){
                    int position = cropViewHolder.selectedPosition;
                    gridViewAdapter.updateUriForPosition(position, newUri);

                    System.out.println("HEHEEHE");
                    System.out.println(gridViewAdapter.retrieveAllRes());
                }
            }
        });



    }
}
