package customUI.ImageView;

import android.support.annotation.Nullable;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.views.image.ReactImageView;

/**
 * Created by sitongchen on 2018-03-27.
 */

public class ImageCropViewManager extends SimpleViewManager<ImageCropView> {
    public static final String REACT_CLASS = "ImageCropView";


    @ReactProp(name = "imageUrlList")
    public void setImageUrlList(ImageCropView view, ReadableArray sources) {
        view.setImageUrlList(sources.toArrayList());
    }


    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected ImageCropView createViewInstance(ThemedReactContext reactContext) {
        return new ImageCropView(reactContext);
    }
}
