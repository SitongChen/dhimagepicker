package customUI.ImageView;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.image_picker.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;


public class GridViewAdapter extends RecyclerView.Adapter<GridViewAdapter.ViewHolder>{
    public interface OnItemClickListener {
        void onItemClick(Bitmap bitmap, Uri uri, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        // Internal class for async images loading.
        class BitmapGetter extends AsyncTask<Uri, Void, Bitmap> {

            public BitmapGetter(){

            }
            @Override
            protected Bitmap doInBackground(Uri... uris) {

                if (uris.length < 1){
                    return null;
                }
                Uri uri = uris[0];
                File file = new File(uri.getPath());
                if (file.exists()){
                    System.out.println("LoadImage");

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                    options.inSampleSize = ImageProcesser.calculateInSampleSize(options, CELLWIDTH , CELLWIDTH);
                    options.inJustDecodeBounds = false;
                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);

                    return bitmap;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (currentBitmap != null){
                    currentBitmap.recycle();
                }
                currentBitmap = bitmap;
                imageView.setImageBitmap(bitmap);
            }
        }
        public ImageView imageView;
        public Uri imageUri;
        public Bitmap currentBitmap;

        public ViewHolder(ImageView IV ){
            super(IV);
            imageView = IV;
        }

        public void bind( Uri uri , final GridViewAdapter.OnItemClickListener listener, final int position){
            imageUri = uri;

            new BitmapGetter().execute(imageUri);

//            imageView.setImageBitmap(bitmap);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (currentBitmap != null && imageUri != null) {
                        listener.onItemClick(currentBitmap, imageUri, position);
                    }
                }
            });

        }
    }


    private Integer[] ImageList = {
            R.drawable.sample_0, R.drawable.sample_1, R.drawable.sample_2, R.drawable.sample_3, R.drawable.sample_4,R.drawable.sample_5,R.drawable.sample_6, R.drawable.sample_7
    };
    public int CELLWIDTH = Resources.getSystem().getDisplayMetrics().widthPixels / 3 - 1;

    private Context context;


    private ArrayList<Uri> uriArrayList = new ArrayList<>();


    private final GridViewAdapter.OnItemClickListener clickListener;


    public GridViewAdapter(Context c, GridViewAdapter.OnItemClickListener listener){
        context = c;
        clickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        System.out.println("onCreateViewHolder");
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new GridView.LayoutParams(CELLWIDTH, CELLWIDTH));
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setPadding(2, 2, 2, 2);

        ViewHolder vh = new ViewHolder(imageView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        System.out.println(uriArrayList);

        System.out.println("onBindViewHolder " + position);

        holder.bind( uriArrayList.get(position) , clickListener, position);
    }

    @Override
    public int getItemCount() {
        return uriArrayList.size();
    }



    public void updateWholeUriList(ArrayList<Uri> list){
        uriArrayList = list;
        notifyDataSetChanged();
    }

    public void updateUriForPosition(int position, Uri newUri){
        uriArrayList.set(position, newUri);
        notifyItemChanged(position);
    }

    public ArrayList<String> retrieveAllRes(){
        ArrayList<String> stringArrayList = new ArrayList<>();

        for (int i = 0; i < uriArrayList.size(); i++ ){
            stringArrayList.add( uriArrayList.get(i).toString());
        }

        removeBuffer();

        return stringArrayList;
    }

    public void removeBuffer(){
        String internalFilePath = this.context.getFilesDir().getAbsolutePath();

        System.out.println("internalFilePath : " + internalFilePath);
        for(int i = 0; i < uriArrayList.size(); i ++){
            Uri uri = uriArrayList.get(i);
            System.out.println(uri.getPath());
        }
    }

}