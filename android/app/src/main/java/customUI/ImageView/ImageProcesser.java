package customUI.ImageView;

import android.graphics.BitmapFactory;

/**
 * Created by sitongchen on 2018-03-28.
 */

public class ImageProcesser extends Object {

    public static int calculateInSampleSize(BitmapFactory.Options options, int requireWidth, int requireHeight){
        int imgWidth = options.outWidth;
        int imgHeiht = options.outHeight;

        int sampleSize = 0;

        int widthRatio = (int) Math.ceil( (imgWidth * 1.0f) / requireWidth);
        int heightRatio = (int) Math.ceil( (imgHeiht * 1.0f) / requireHeight);

        sampleSize = Math.max(widthRatio, heightRatio);

        return sampleSize;

    }
}
