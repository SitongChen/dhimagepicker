package customUI.ImageView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by sitongchen on 2018-03-29.
 */

public class UpdatedRecycleView extends RecyclerView {
    private boolean mRequestedLayout = false;

    public UpdatedRecycleView(Context context) {
        super(context);
    }

    public UpdatedRecycleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UpdatedRecycleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }



//    public void gg(){
//        final GridViewAdapter gridViewAdapter = (GridViewAdapter) getAdapter();
//        mRequestedLayout = false;
//        gridViewAdapter.random();
//    }

    @Override
    public void requestLayout() {
        super.requestLayout();

        if (!mRequestedLayout) {
            mRequestedLayout = true;
            this.post(new Runnable() {
                @Override
                public void run() {
                    mRequestedLayout = false;
                    forceLayout();
                    layout(getLeft(), getTop(), getRight(), getBottom());
                    // Note. the following line is prettttttty important.
                    onLayout(false, getLeft(), getTop(), getRight(), getBottom());
                }
            });
        }
    }
}
